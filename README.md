# coviduino

Arduino-based Air Quality Measuring Device with SCD30 Sensor and Neopixels

## 💡 Documentation

None yet.

## 🚀 Installation

## 🔗 Dependencies

FastLED and SCD30 Library

## 👥 Contributing

Please get in touch with us at (office@hampel-soft.com) or visit our website (www.hampel-soft.com) if you want to contribute.

## 🍻 Credits

* Bence Bartho

## 📄 License

This repository and its contents are licensed under a BSD/MIT like license - see the LICENSE file for details
