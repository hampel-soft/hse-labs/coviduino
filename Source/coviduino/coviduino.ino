// ============================================================ //
// Includes
// ============================================================ //
// Arduino libraries are managed in three different places: 
// - inside the IDE installation folder
// - inside the core folder
// - in the libraries folder inside your sketchbook
// When the angle brackets syntax is used, the libraries paths 
//   will be searched for the file.
// When the double quotes syntax is used, the folder of the file 
//  using the #include directive will be searched for the specified 
//  file, then the libraries paths if it was not found in the local 
//  path. Use this syntax for header files in the sketch’s folder.

#include <Wire.h>
#include <FastLED.h>
#include <SparkFun_SCD30_Arduino_Library.h>
#include <ThingSpeak.h>
#include <SPI.h>
#include <Ethernet.h>
#include <PubSubClient.h>

// angle brackets: don't look in sketch path, but in libraries path
// Windows: <%Documents%>/Arduino/libraries/credentials/ 
#include <credentials.h>


// ============================================================ //
// Definitions
// ============================================================ //


// ------------------------------------------------------------
// Ethernet
// ------------------------------------------------------------
IPAddress ip(SECRET_IP);
IPAddress myDns(SECRET_DNS);
byte mac[] = SECRET_MAC;

EthernetClient ethClient;

// ------------------------------------------------------------
// Thingspeak
// ------------------------------------------------------------
unsigned long myChannelNumber = SECRET_CH_ID;
const char * myWriteAPIKey = SECRET_WRITE_APIKEY;

//Counter for timing Thingspeak updates
int TSskipCounter = 0;
int TSrunningCounter = 0;

// ------------------------------------------------------------
// MQTT (https://pubsubclient.knolleary.net/api)
// ------------------------------------------------------------
IPAddress mqttServer(SECRET_MQTT_URL);
PubSubClient mqttClient(ethClient);

// ------------------------------------------------------------
// NeoPixels
// ------------------------------------------------------------
#define NUM_LEDS 29
#define DATA_PIN 6
#define LED_TYPE WS2812
#define COLOR_ORDER GRB

CRGB leds[NUM_LEDS];

// ------------------------------------------------------------
// Air Sensor / CO2 measurement limits
// ------------------------------------------------------------
SCD30 airSensor;

const float extremeCO2 = 1400;  //(ppm) literature value found on internet: maximum allowed in mech. vented rooms
const float criticalCO2 = 1000; //(ppm) literature value found on internet: maximum for "good" room air
const float minCO2 = 600;       //(ppm) literature value found on internet: ideal for mechanically vented air
const float referenceCO2 = 400; //(ppm) literature value found on internet: still, fresh air

const float minRoomTemp = 20;   // "boss key": Close window if colder than this!

int AIRrunningCounter = 0;


// ============================================================ //
// SETUP
// ============================================================ //
void setup()
{
  Serial.begin(115200);
  Serial.println();
  Serial.println("---------- HSE COVIDuino ----------");
  Serial.println("Should I ventilate? Let's find out!");
  Serial.println("-----------------------------------");

  // ************************************************************ //
  // Ethernet
  // ************************************************************ //
  Ethernet.init(10);
  // start the Ethernet connection:
  Serial.println("Initialize Ethernet with DHCP:");
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // Check for Ethernet hardware present
    if (Ethernet.hardwareStatus() == EthernetNoHardware) {
      Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
      while (true) {
        delay(1); // do nothing, no point running without Ethernet hardware
      }
    }
    if (Ethernet.linkStatus() == LinkOFF) {
      Serial.println("Ethernet cable is not connected.");
    }
    // try to congifure using IP address instead of DHCP:
    Ethernet.begin(mac, ip, myDns);
  } else {
    Serial.print("  DHCP assigned IP ");
    Serial.println(Ethernet.localIP());
  }
  // give the Ethernet shield a second to initialize:
  delay(1000);


  // ************************************************************ //
  // Thingspeak
  // ************************************************************ //
  ThingSpeak.begin(ethClient);  // Initialize ThingSpeak
  Wire.begin();
  if (airSensor.begin() == false)
  {
    Serial.println("Air sensor not detected. (Did you check if there are dedicated SDA/SCA pins? ;) )");
  }

  
  // ************************************************************ //
  // MQTT
  // ************************************************************ //

  mqttClient.setServer(mqttServer, SECRET_MQTT_PORT);
  mqttClient.setCallback(mqttCallback);



  // ************************************************************ //
  // LEDs
  // ************************************************************ //
  FastLED.addLeds<LED_TYPE, DATA_PIN, COLOR_ORDER>(leds, NUM_LEDS);
  for (int i = 0; i < NUM_LEDS; i++) {
    FastLED.setBrightness(i * 5);
    setLeds(i, CRGB::Purple, CRGB::Black);
    delay(50);
  }
  for (int i = NUM_LEDS; i > 0; i--) {
    FastLED.setBrightness(i * 5);
    setLeds(i, CRGB::Purple, CRGB::Black);
    delay(50);
  }
  FastLED.setBrightness(30);
  
  // Finished setup.
  Serial.println("Setup complete.\n");
}


// ============================================================ //
// MAIN LOOP
// ============================================================ //
void loop()
{
  //increase Thingspeak delay counter)
  TSskipCounter++;

  // ************************************************************ //
  // SENSOR
  // ************************************************************ //
  if (airSensor.dataAvailable())
  {
    float carbdiox = airSensor.getCO2();
    float temper = airSensor.getTemperature();
    float hum =  airSensor.getHumidity();

    Serial.print("[");
    Serial.print(AIRrunningCounter++);
    Serial.print("] co2(ppm):");
    Serial.print(carbdiox);
    Serial.print(" temp(C):");
    Serial.print(temper, 1);
    Serial.print(" humidity(%):");
    Serial.print(hum, 1);
    Serial.print("\n");


    // ************************************************************ //
    // PUBLISH TO THINGSPEAK
    // ************************************************************ //
    if (TSskipCounter >= 15) {
      TSskipCounter = 0;
      ThingSpeak.setField(1, carbdiox);
      ThingSpeak.setField(2, temper);
      ThingSpeak.setField(3, hum);
      ThingSpeak.setStatus("test");

      // write to the ThingSpeak channel
      Serial.print("\nThingspeak channel update #");
      Serial.print(TSrunningCounter++);
      Serial.print("... ");
      int x = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
      if (x == 200) {
        Serial.println("successful.");
      }
      else {
        Serial.println("failed: Return code = " + String(x));
      }
    }

    // ************************************************************ //
    // PUBLISH TO MQTT
    // ************************************************************ //
    
    #define MSG_BUFFER_SIZE  (100)
    char msg[MSG_BUFFER_SIZE];

    String message = String("weather,location=de temperature="+String(temper));
    message.toCharArray(msg, message.length());
    mqttClient.publish("iTemperature", msg, true);
    
    message = String("weather,location=de humidity="+String(hum));
    message.toCharArray(msg, message.length());
    mqttClient.publish("iHumidity", msg, true);
    
    message = String("weather,location=de carbdiox="+String(carbdiox));
    message.toCharArray(msg, message.length());
    mqttClient.publish("iCarbdiox", msg, true);
    
    //mqttClient.publish("covid_Temperature", String(temper).c_str(), true);
    //mqttClient.publish("covid_Humidity", String(hum).c_str(), true);
    //mqttClient.publish("covid_Carbdiox", String(carbdiox).c_str(), true);



    // ************************************************************ //
    // NEOPIXEL LEDS
    // ************************************************************ //

    // ------------------------------------------------------------ //
    // FREEEZING!!
    if (airSensor.getTemperature() <= minRoomTemp) {
      setLeds(0, CRGB::DarkBlue, CRGB::DarkBlue);
    }
    else {

      // ------------------------------------------------------------ //
      // reference (fresh air)
      if (carbdiox <= referenceCO2) {
        setLeds(0, CRGB::White, CRGB::White);
      }

      // ------------------------------------------------------------ //
      // greater than fresh air, less than indoor minimum
      else if (carbdiox > referenceCO2 && carbdiox < minCO2) {
        int greenLEDs = (carbdiox - referenceCO2) / ((minCO2 - referenceCO2) / NUM_LEDS);
        setLeds(greenLEDs, CRGB::Green, CRGB::White);
      }

      // ------------------------------------------------------------ //
      // indoor minimum
      else if (carbdiox == minCO2) {
        setLeds(0, CRGB::Green, CRGB::Green);
      }

      // ------------------------------------------------------------ //
      // greater than min, less than critical
      else if (carbdiox > minCO2 && carbdiox < criticalCO2) {
        int amberLEDs = (carbdiox - minCO2) / ((criticalCO2 - minCO2) / NUM_LEDS);
        setLeds(amberLEDs, CRGB::OrangeRed, CRGB::Green);
      }

      // ------------------------------------------------------------ //
      // Critical
      else if (carbdiox == criticalCO2) {
        setLeds(0, CRGB::OrangeRed, CRGB::OrangeRed);
      }

      // ------------------------------------------------------------ //
      // between critical and extreme
      else if (carbdiox > criticalCO2 && carbdiox < extremeCO2) {
        int redLEDs = (carbdiox - criticalCO2) / ((extremeCO2 - criticalCO2) / NUM_LEDS);
        setLeds(redLEDs, CRGB::Red, CRGB::OrangeRed);
      }

      // ------------------------------------------------------------ //
      // EXTREME!!
      else {
        setLeds(0, CRGB::Red, CRGB::Red);
        delay(1000);
        setLeds(0, CRGB::Black, CRGB::Black);
      }
    }


    Serial.println();
  }
  //else
  //  Serial.println(".\n");

  if (!mqttClient.connected()) {
    mqttReconnect();
  }
  mqttClient.loop();

  delay(1000);
}

// ============================================================ //
// MQTT functions
// ============================================================ //

void mqttCallback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i=0;i<length;i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}


void mqttReconnect() {
  // Loop until we're reconnected
  if (!mqttClient.connected()) {
    Serial.print("Attempting MQTT connection... \n");
    // Attempt to connect
    if (mqttClient.connect("arduinoClient", SECRET_MQTT_USER, SECRET_MQTT_PWD)) {
      Serial.println("connected \n");
      // Once connected, publish an announcement...
      mqttClient.publish("outTopic","hello world");
      // ... and resubscribe
      mqttClient.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
    }
  }
}

// ============================================================ //
// Update LEDs - Helper
// ============================================================ //
void setLeds(int bottomCount, const CRGB bottomColor, const CRGB topColor) {
  Serial.println(debugOutput(bottomCount, bottomColor, topColor));
  if (bottomCount > 0) {
    for (int i = 0; i < bottomCount; i++) {
      leds[i] = bottomColor;
    }
  }
  if (bottomCount < NUM_LEDS) {
    for (int i = bottomCount; i < NUM_LEDS; i++) {
      leds[i] = topColor;
    }
  }
  FastLED.show();
}



// ============================================================ //
// Debug Output for CRGB - Helper
// ============================================================ //
String debugOutput(int bottomCount, const CRGB bottomColor, const CRGB topColor) {
  int bColR = bottomColor.r;
  int bColG = bottomColor.g;
  int bColB = bottomColor.b;
  int tColR = topColor.r;
  int tColG = topColor.g;
  int tColB = topColor.b;
  String debugOut = "---> ";
  debugOut += bottomCount;
  debugOut += " x ";
  debugOut += bColR;
  debugOut += "/";
  debugOut += bColG;
  debugOut += "/";
  debugOut += bColB;
  debugOut += "; rest ";
  debugOut += tColR;
  debugOut += "/";
  debugOut += tColG;
  debugOut += "/";
  debugOut += tColB;
  return debugOut;
}
